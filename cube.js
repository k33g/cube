
console.log("📦 Cube Made with ❤️")

class Cube extends HTMLElement {
  
  get owner() {
    // access to the static property `owner`
    return this.constructor.owner
  }
  
  get template() {
    throw "😡 you have to define a template property"
  }

  constructor() {
    super()
    console.log("⚡️ constructor", this)
  }

  connectedCallback() {
    console.log("⚡️ connectedCallback", this)
    const shadowRoot = this.attachShadow({mode: 'open'});
    const template = this.owner.querySelector(this.template);
    const instance = template.content.cloneNode(true);
    
    shadowRoot.appendChild(instance);
    
    if(this.connected) { this.connected() } else { throw "😡 you have to define a connected method" }

  }

  static register({owner, tag, component}) {
    (function () {
      // static property: `owner`
      //component.owner = owner
      owner == undefined ? component.owner = document.currentScript.ownerDocument : component.owner = owner
      customElements.define(tag, component)
    })()
  }
}

