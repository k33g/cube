# 📦 Cube

## What?

> WIP 🏗

> Web Components experiments

## Use it (but not in production)

### Setup

> with recent version of Chrome:

```html
<script src="cube.js"></script>
```

> with recent version of Safari, FireFox, Edge(not tested), you need polyfills:

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/1.1.0/webcomponents-hi-sd-ce.js"></script>
<script src="cube.js"></script>
```

### Define a webcomponent

> nice-title.html

```html
<template id="nice-title-template">
  <link rel="stylesheet" href="/demo.components/nice-title.css">
  <div>
    <h1 class="orange_title">Hello 👋 🌍 🦊</h1>
    <slot></slot>
  </div>
  
</template>
<script>

  class NiceTitle extends Cube {

    get template() {
      return "#nice-title-template"
    }
    constructor() {
      super()
    }
    connected() {
      console.log("connected!")
    }
  }

  Cube.register({tag: "nice-title", component: NiceTitle})
</script>
```

> use it in a html page:

```html
<head>
  <script src="cube.js"></script>
  <link rel="import" href="./demo.components/nice-title.html">
</head>
<body>
  <nice-title>😍</nice-title>
</body>
```

## Play with Cube

- Install https://github.com/indexzero/http-server
- Launch `http-server -c-1` *(`-c-1` is to disable the cache)*
- Open http://127.0.0.1:8080/demo.html


## Resources

- https://ayushgp.github.io/html-web-components-using-vanilla-js/
- https://ayushgp.github.io/html-web-components-using-vanilla-js-part-2/
- https://ayushgp.github.io/html-web-components-using-vanilla-js-part-3/
- https://developers.google.com/web/fundamentals/web-components/customelements
- https://www.codementor.io/ayushgupta/vanilla-js-web-components-chguq8goz
- https://auth0.com/blog/web-components-how-to-craft-your-own-custom-components/
- https://developers.google.com/web/fundamentals/web-components/customelements
- https://alligator.io/web-components/composing-slots-named-slots/


